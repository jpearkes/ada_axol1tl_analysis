# ada_axol1tl_analysis



## Getting started
To get started we will run this notebook on the LPC. In your shell run the following commands replacing "your username" with your Fermilab login: 

```
kinit
ssh -L localhost:8888:localhost:8888 your_username@cmslpc-el9.fnal.gov
```
This should allow you to successfully log in to the LPC. 

If that worked, next run:

```
source /cvmfs/cms.cern.ch/cmsset_default.sh
cmsrel CMSSW_14_0_7
cd CMSSW_14_0_7/
cmsenv
```
This will set up your computing environment with everything you'll need to get stated. Next we'll use git to pull this repository to your local directory. 

```
cd src
git pull https://gitlab.cern.ch/jpearkes/ada_axol1tl_analysis
ls
```
You should now be able to see this repository in your local directory. Let's go see what is inside:
```
cd ada_axol1tl_analysis
ls
```
Next we'll use port-forwarding to open up the notebook: 
```
jupyter notebook --no-browser --port=8888 --ip 127.0.0.1
```
Copy the first url under  “Or copy and paste one of these URLs:” and paste it into your webrowser on the laptop

More information about using the command line can be found under the UNIX Shell here: https://hsf-training.org/training-center/

## Once things are set up
Once the repository has been set up, you won't need to pull each time you log in. Instead you can just run:

```
kinit
ssh -L localhost:8888:localhost:8888 your_username@cmslpc-el9.fnal.gov
source /cvmfs/cms.cern.ch/cmsset_default.sh
cmsrel CMSSW_14_0_7
cd CMSSW_14_0_7/
cmsenv
cd src/ada_axol1tl_analysis
jupyter notebook --no-browser --port=8888 --ip 127.0.0.1
```
And copy the url into your browser. 

## Next steps
Each of the notebooks contains some information for getting started with analyzing axol1tl data. Work through them at your own pace and ask questions whenever you run into something you don't understand.  

## Resources

https://hsf-training.github.io/hsf-training-scikit-hep-webpage/
